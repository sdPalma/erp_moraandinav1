package moraandinademo.model.inventario.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.managers.ManagerDAO;
import moraandinademo.model.core.entities.VntProducto;

/**
 * Session Bean implementation class ManagerInventario
 */
@Stateless
@LocalBean
public class ManagerInventario {

	@EJB
	private ManagerDAO mDAO;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public ManagerInventario() {
		// TODO Auto-generated constructor stub
	}

	public List<VntProducto> findAllVntProducto() {
		TypedQuery<VntProducto> q = em.createQuery("SELECT v FROM VntProducto v order by v.idProducto",
				VntProducto.class);
		return q.getResultList();
	}

	public VntProducto findByIdVntProducto(int idProducto) throws Exception {
		return (VntProducto) mDAO.findById(VntProducto.class, idProducto);
	}

	
	public void InsertarProducto(VntProducto nuevoProducto) throws Exception {
		mDAO.insertar(nuevoProducto);
	}

	public void actualizarProducto(VntProducto edicionProducto) throws Exception {
		VntProducto Producto = (VntProducto) mDAO.findById(VntProducto.class, edicionProducto.getIdProducto());
		Producto.setDescripcion(edicionProducto.getDescripcion());
		Producto.setCantidad(edicionProducto.getCantidad());
		mDAO.actualizar(Producto);
		// mAuditoria.mostrarLog(loginDTO, getClass(), "actualizarUsuario", "se
		// actualizó al usuario "+Proveedor.getNombre());
	}

	public void eliminarProducto(int idProducto) throws Exception {
		VntProducto Producto = (VntProducto) mDAO.findById(VntProducto.class, idProducto);
		mDAO.eliminar(VntProducto.class, Producto.getIdProducto());
		// TODO agregar uso de LoginDTO para auditar metodo.
	}

}
