package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the prv_compra_detalle database table.
 * 
 */
@Entity
@Table(name="prv_compra_detalle")
@NamedQuery(name="PrvCompraDetalle.findAll", query="SELECT p FROM PrvCompraDetalle p")
public class PrvCompraDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_prv_compra_detalle", unique=true, nullable=false)
	private Integer idPrvCompraDetalle;

	@Column(nullable=false)
	private Integer cantidad;

	private double costo;

	@Column(nullable=false)
	private double subtotal;

	//bi-directional many-to-one association to PrvCompraCabecera
	@ManyToOne
	@JoinColumn(name="id_prv_compra_cabecera", nullable=false)
	private PrvCompraCabecera prvCompraCabecera;

	//bi-directional many-to-one association to VntProducto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private VntProducto vntProducto;

	public PrvCompraDetalle() {
	}

	public Integer getIdPrvCompraDetalle() {
		return this.idPrvCompraDetalle;
	}

	public void setIdPrvCompraDetalle(Integer idPrvCompraDetalle) {
		this.idPrvCompraDetalle = idPrvCompraDetalle;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public double getCosto() {
		return this.costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public PrvCompraCabecera getPrvCompraCabecera() {
		return this.prvCompraCabecera;
	}

	public void setPrvCompraCabecera(PrvCompraCabecera prvCompraCabecera) {
		this.prvCompraCabecera = prvCompraCabecera;
	}

	public VntProducto getVntProducto() {
		return this.vntProducto;
	}

	public void setVntProducto(VntProducto vntProducto) {
		this.vntProducto = vntProducto;
	}

}