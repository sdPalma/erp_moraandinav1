package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_proveedor database table.
 * 
 */
@Entity
@Table(name="inv_proveedor")
@NamedQuery(name="InvProveedor.findAll", query="SELECT i FROM InvProveedor i")
public class InvProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ruc_proveedor", unique=true, nullable=false, length=100)
	private String rucProveedor;

	@Column(nullable=false, length=150)
	private String correo;

	@Column(nullable=false, length=100)
	private String direccion;

	@Column(nullable=false, length=100)
	private String nombre;

	@Column(nullable=false, length=10)
	private String telefono;

	//bi-directional many-to-one association to PrvCompraCabecera
	@OneToMany(mappedBy="invProveedor")
	private List<PrvCompraCabecera> prvCompraCabeceras;

	public InvProveedor() {
	}

	public String getRucProveedor() {
		return this.rucProveedor;
	}

	public void setRucProveedor(String rucProveedor) {
		this.rucProveedor = rucProveedor;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<PrvCompraCabecera> getPrvCompraCabeceras() {
		return this.prvCompraCabeceras;
	}

	public void setPrvCompraCabeceras(List<PrvCompraCabecera> prvCompraCabeceras) {
		this.prvCompraCabeceras = prvCompraCabeceras;
	}

	public PrvCompraCabecera addPrvCompraCabecera(PrvCompraCabecera prvCompraCabecera) {
		getPrvCompraCabeceras().add(prvCompraCabecera);
		prvCompraCabecera.setInvProveedor(this);

		return prvCompraCabecera;
	}

	public PrvCompraCabecera removePrvCompraCabecera(PrvCompraCabecera prvCompraCabecera) {
		getPrvCompraCabeceras().remove(prvCompraCabecera);
		prvCompraCabecera.setInvProveedor(null);

		return prvCompraCabecera;
	}

}