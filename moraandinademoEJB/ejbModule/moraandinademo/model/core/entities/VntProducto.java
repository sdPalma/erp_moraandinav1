package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vnt_producto database table.
 * 
 */
@Entity
@Table(name="vnt_producto")
@NamedQuery(name="VntProducto.findAll", query="SELECT v FROM VntProducto v")
public class VntProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_producto", unique=true, nullable=false)
	private Integer idProducto;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false, length=100)
	private String descripcion;

	//bi-directional many-to-one association to PrvCompraDetalle
	@OneToMany(mappedBy="vntProducto")
	private List<PrvCompraDetalle> prvCompraDetalles;

	//bi-directional many-to-one association to VndVentaDetalle
	@OneToMany(mappedBy="vntProducto")
	private List<VndVentaDetalle> vndVentaDetalles;

	public VntProducto() {
	}

	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<PrvCompraDetalle> getPrvCompraDetalles() {
		return this.prvCompraDetalles;
	}

	public void setPrvCompraDetalles(List<PrvCompraDetalle> prvCompraDetalles) {
		this.prvCompraDetalles = prvCompraDetalles;
	}

	public PrvCompraDetalle addPrvCompraDetalle(PrvCompraDetalle prvCompraDetalle) {
		getPrvCompraDetalles().add(prvCompraDetalle);
		prvCompraDetalle.setVntProducto(this);

		return prvCompraDetalle;
	}

	public PrvCompraDetalle removePrvCompraDetalle(PrvCompraDetalle prvCompraDetalle) {
		getPrvCompraDetalles().remove(prvCompraDetalle);
		prvCompraDetalle.setVntProducto(null);

		return prvCompraDetalle;
	}

	public List<VndVentaDetalle> getVndVentaDetalles() {
		return this.vndVentaDetalles;
	}

	public void setVndVentaDetalles(List<VndVentaDetalle> vndVentaDetalles) {
		this.vndVentaDetalles = vndVentaDetalles;
	}

	public VndVentaDetalle addVndVentaDetalle(VndVentaDetalle vndVentaDetalle) {
		getVndVentaDetalles().add(vndVentaDetalle);
		vndVentaDetalle.setVntProducto(this);

		return vndVentaDetalle;
	}

	public VndVentaDetalle removeVndVentaDetalle(VndVentaDetalle vndVentaDetalle) {
		getVndVentaDetalles().remove(vndVentaDetalle);
		vndVentaDetalle.setVntProducto(null);

		return vndVentaDetalle;
	}

}