package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the prv_compra_cabecera database table.
 * 
 */
@Entity
@Table(name="prv_compra_cabecera")
@NamedQuery(name="PrvCompraCabecera.findAll", query="SELECT p FROM PrvCompraCabecera p")
public class PrvCompraCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_prv_compra_cabecera", unique=true, nullable=false)
	private Integer idPrvCompraCabecera;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_compra", nullable=false)
	private Date fechaCompra;

	@Column(name="total_compra", nullable=false)
	private double totalCompra;

	//bi-directional many-to-one association to InvProveedor
	@ManyToOne
	@JoinColumn(name="ruc_proveedor", nullable=false)
	private InvProveedor invProveedor;

	//bi-directional many-to-one association to PrvCompraDetalle
	@OneToMany(mappedBy="prvCompraCabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<PrvCompraDetalle> prvCompraDetalles;

	public PrvCompraCabecera() {
	}

	public Integer getIdPrvCompraCabecera() {
		return this.idPrvCompraCabecera;
	}

	public void setIdPrvCompraCabecera(Integer idPrvCompraCabecera) {
		this.idPrvCompraCabecera = idPrvCompraCabecera;
	}

	public Date getFechaCompra() {
		return this.fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public double getTotalCompra() {
		return this.totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public InvProveedor getInvProveedor() {
		return this.invProveedor;
	}

	public void setInvProveedor(InvProveedor invProveedor) {
		this.invProveedor = invProveedor;
	}

	public List<PrvCompraDetalle> getPrvCompraDetalles() {
		return this.prvCompraDetalles;
	}

	public void setPrvCompraDetalles(List<PrvCompraDetalle> prvCompraDetalles) {
		this.prvCompraDetalles = prvCompraDetalles;
	}

	public PrvCompraDetalle addPrvCompraDetalle(PrvCompraDetalle prvCompraDetalle) {
		getPrvCompraDetalles().add(prvCompraDetalle);
		prvCompraDetalle.setPrvCompraCabecera(this);

		return prvCompraDetalle;
	}

	public PrvCompraDetalle removePrvCompraDetalle(PrvCompraDetalle prvCompraDetalle) {
		getPrvCompraDetalles().remove(prvCompraDetalle);
		prvCompraDetalle.setPrvCompraCabecera(null);

		return prvCompraDetalle;
	}

}