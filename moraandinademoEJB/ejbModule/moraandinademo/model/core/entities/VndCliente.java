package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vnd_cliente database table.
 * 
 */
@Entity
@Table(name="vnd_cliente")
@NamedQuery(name="VndCliente.findAll", query="SELECT v FROM VndCliente v")
public class VndCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false, length=10)
	private String cedula;

	@Column(nullable=false, length=100)
	private String direccion;

	@Column(nullable=false, length=100)
	private String nombre;

	@Column(nullable=false, length=10)
	private String telefono;

	//bi-directional many-to-one association to VndVentaCabecera
	@OneToMany(mappedBy="vndCliente")
	private List<VndVentaCabecera> vndVentaCabeceras;

	public VndCliente() {
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<VndVentaCabecera> getVndVentaCabeceras() {
		return this.vndVentaCabeceras;
	}

	public void setVndVentaCabeceras(List<VndVentaCabecera> vndVentaCabeceras) {
		this.vndVentaCabeceras = vndVentaCabeceras;
	}

	public VndVentaCabecera addVndVentaCabecera(VndVentaCabecera vndVentaCabecera) {
		getVndVentaCabeceras().add(vndVentaCabecera);
		vndVentaCabecera.setVndCliente(this);

		return vndVentaCabecera;
	}

	public VndVentaCabecera removeVndVentaCabecera(VndVentaCabecera vndVentaCabecera) {
		getVndVentaCabeceras().remove(vndVentaCabecera);
		vndVentaCabecera.setVndCliente(null);

		return vndVentaCabecera;
	}

}