package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the vnd_venta_cabecera database table.
 * 
 */
@Entity
@Table(name = "vnd_venta_cabecera")
@NamedQuery(name = "VndVentaCabecera.findAll", query = "SELECT v FROM VndVentaCabecera v")
public class VndVentaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_vnd_venta_cabecera", unique = true, nullable = false)
	private Integer idVndVentaCabecera;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_venta", nullable = false)
	private Date fechaVenta;

	private double iva;

	private double subtotal;

	@Column(nullable = false)
	private double total;

	// bi-directional many-to-one association to VndCliente
	@ManyToOne
	@JoinColumn(name = "cedula_cliente", nullable = false)
	private VndCliente vndCliente;

	// bi-directional many-to-one association to VndVentaDetalle
	@OneToMany(mappedBy = "vndVentaCabecera", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<VndVentaDetalle> vndVentaDetalles;

	public VndVentaCabecera() {
	}

	public Integer getIdVndVentaCabecera() {
		return this.idVndVentaCabecera;
	}

	public void setIdVndVentaCabecera(Integer idVndVentaCabecera) {
		this.idVndVentaCabecera = idVndVentaCabecera;
	}

	public Date getFechaVenta() {
		return this.fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public double getIva() {
		return this.iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public VndCliente getVndCliente() {
		return this.vndCliente;
	}

	public void setVndCliente(VndCliente vndCliente) {
		this.vndCliente = vndCliente;
	}

	public List<VndVentaDetalle> getVndVentaDetalles() {
		return this.vndVentaDetalles;
	}

	public void setVndVentaDetalles(List<VndVentaDetalle> vndVentaDetalles) {
		this.vndVentaDetalles = vndVentaDetalles;
	}

	public VndVentaDetalle addVndVentaDetalle(VndVentaDetalle vndVentaDetalle) {
		getVndVentaDetalles().add(vndVentaDetalle);
		vndVentaDetalle.setVndVentaCabecera(this);

		return vndVentaDetalle;
	}

	public VndVentaDetalle removeVndVentaDetalle(VndVentaDetalle vndVentaDetalle) {
		getVndVentaDetalles().remove(vndVentaDetalle);
		vndVentaDetalle.setVndVentaCabecera(null);

		return vndVentaDetalle;
	}

}