package moraandinademo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the vnd_venta_detalle database table.
 * 
 */
@Entity
@Table(name="vnd_venta_detalle")
@NamedQuery(name="VndVentaDetalle.findAll", query="SELECT v FROM VndVentaDetalle v")
public class VndVentaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_vnd_venta_detalle", unique=true, nullable=false)
	private Integer idVndVentaDetalle;

	@Column(nullable=false)
	private Integer cantidad;

	@Column(nullable=false)
	private double costo;

	@Column(nullable=false)
	private double subtotal;

	//bi-directional many-to-one association to VndVentaCabecera
	@ManyToOne
	@JoinColumn(name="id_vnd_venta_cabecera", nullable=false)
	private VndVentaCabecera vndVentaCabecera;

	//bi-directional many-to-one association to VntProducto
	@ManyToOne
	@JoinColumn(name="id_producto", nullable=false)
	private VntProducto vntProducto;

	public VndVentaDetalle() {
	}

	public Integer getIdVndVentaDetalle() {
		return this.idVndVentaDetalle;
	}

	public void setIdVndVentaDetalle(Integer idVndVentaDetalle) {
		this.idVndVentaDetalle = idVndVentaDetalle;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public double getCosto() {
		return this.costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public VndVentaCabecera getVndVentaCabecera() {
		return this.vndVentaCabecera;
	}

	public void setVndVentaCabecera(VndVentaCabecera vndVentaCabecera) {
		this.vndVentaCabecera = vndVentaCabecera;
	}

	public VntProducto getVntProducto() {
		return this.vntProducto;
	}

	public void setVntProducto(VntProducto vntProducto) {
		this.vntProducto = vntProducto;
	}

}