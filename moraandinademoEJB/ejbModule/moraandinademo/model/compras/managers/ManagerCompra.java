package moraandinademo.model.compras.managers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import moraandinademo.model.compras.dtos.DTODetalleCompra;
import moraandinademo.model.core.entities.*;






/**
 * Session Bean implementation class ManagerCompra
 */
@Stateless
@LocalBean
public class ManagerCompra {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ManagerCompra() {
        
    }
    
    public List<InvProveedor> findAllInvProveedor(){
    	TypedQuery<InvProveedor> q = em.createQuery("select p from InvProveedor p order by p.nombre", InvProveedor.class);
    	return q.getResultList();
    }
    
    public List<VntProducto> findAllVtnProducto(){
    	TypedQuery<VntProducto> q = em.createQuery("select p from VntProducto p order by p.descripcion", VntProducto.class);
    	return q.getResultList();
    }
    
    
    public List<PrvCompraCabecera> findAllPrvCompraCabeceras(){
    	TypedQuery<PrvCompraCabecera> q = em.createQuery("select p from PrvCompraCabecera p order by p.idPrvCompraCabecera desc", PrvCompraCabecera.class);
    	return q.getResultList();
    }
    
    
    public DTODetalleCompra crearDetalleCompra(int idProducto, int cantidad, double precio) {
    	VntProducto a=em.find(VntProducto.class, idProducto);
    	//a.setCantidad(cantidad);
    	
    	DTODetalleCompra detalle=new DTODetalleCompra();
    	detalle.setProducto(idProducto);
    	
    	detalle.setNombre(a.getDescripcion());
    	detalle.setCantidad(cantidad);
    	detalle.setCosto(precio);
    	detalle.setSubtotal(cantidad*precio);
    	
    	return detalle;
    }
   
    
    
    public double totalDetalleCompra(List<DTODetalleCompra> listaDetalleCompra) {
    	double total=0;
    	for(DTODetalleCompra d:listaDetalleCompra)
    		total+=d.getSubtotal();
    	return total;
    }
    
    public void productosMultiples(List<DTODetalleCompra> listaDetalleCompra, String rucPro) {
    	InvProveedor proveedor=em.find(InvProveedor.class, rucPro);
   
    	PrvCompraCabecera ccab=new PrvCompraCabecera();

    	ccab.setInvProveedor(proveedor);
    	ccab.setFechaCompra(new Date());
    	ccab.setTotalCompra(totalDetalleCompra(listaDetalleCompra));
    	
    	List<PrvCompraDetalle> listaDetalle=new ArrayList<PrvCompraDetalle>();
    	ccab.setPrvCompraDetalles(listaDetalle);
    	
    	for(DTODetalleCompra det:listaDetalleCompra) {
    		PrvCompraDetalle comDet=new PrvCompraDetalle();
    		comDet.setPrvCompraCabecera(ccab);
    		VntProducto produ=em.find(VntProducto.class, det.getProducto());
    		comDet.setVntProducto(produ);
    		comDet.setCantidad(det.getCantidad());
    		produ.setCantidad(produ.getCantidad()+det.getCantidad());
    		comDet.setCosto(det.getCosto());
    		comDet.setSubtotal(det.getSubtotal());
    		
    		listaDetalle.add(comDet);
    	}
    	em.persist(ccab);
    	
    }

    
}
