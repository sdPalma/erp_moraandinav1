package moraandinademo.model.compras.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import minimarketdemo.model.core.managers.ManagerDAO;
import moraandinademo.model.core.entities.InvProveedor;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {

	@PersistenceContext
	private EntityManager em;
	

	@EJB
	private ManagerDAO mDAO;
	

	/**
	 * Default constructor.
	 */
	public ManagerProveedor() {
		// TODO Auto-generated constructor stub
	}

	public void insertarProveedor(InvProveedor nuevoProveedor) throws Exception {
		em.persist(nuevoProveedor);
	}

	public List<InvProveedor> findAllInvProveedor() {
		return em.createNamedQuery("InvProveedor.findAll", InvProveedor.class).getResultList();
	}

	public void eliminarProveedor(String idInProveedore) throws Exception {
		InvProveedor Proveedor = (InvProveedor) mDAO.findById(InvProveedor.class, idInProveedore);
		mDAO.eliminar(InvProveedor.class, Proveedor.getRucProveedor());

	}

	public void actualizarProveedor(InvProveedor edicionProveedor) throws Exception {
		InvProveedor Proveedor = (InvProveedor) mDAO.findById(InvProveedor.class, edicionProveedor.getRucProveedor());

		Proveedor.setRucProveedor(edicionProveedor.getRucProveedor());
		Proveedor.setNombre(edicionProveedor.getNombre());
		Proveedor.setDireccion(edicionProveedor.getDireccion());
		Proveedor.setTelefono(edicionProveedor.getTelefono());
		Proveedor.setCorreo(edicionProveedor.getCorreo());
		mDAO.actualizar(Proveedor);
	}
}
