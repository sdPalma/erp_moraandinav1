package moraandina.model.ventas.managers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.managers.ManagerDAO;
import moraandinademo.model.core.entities.VndCliente;
import moraandinademo.model.core.entities.VndVentaCabecera;
import moraandinademo.model.core.entities.VndVentaDetalle;
import moraandinademo.model.core.entities.VntProducto;
import moraandinademo.model.ventas.dtos.DTODetalleVenta;

/**
 * Session Bean implementation class ManagerVentas
 */
@Stateless
@LocalBean
public class ManagerVentas {
	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManagerDAO mDAO;

	/**
	 * Default constructor.
	 */
	public ManagerVentas() {
		// TODO Auto-generated constructor stub
	}

	// Menu
	public List<VndVentaCabecera> findVentaByFecha(Date fechaInicio, Date fechaFin) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		System.out.println("fecha inicio: " + format.format(fechaInicio));
		System.out.println("fecha fin: " + format.format(fechaFin));
		String consulta = "SELECT v FROM VndVentaCabecera v where v.fechaVenta between :fechaInicio and :fechaFin order by v.fechaVenta ";
		Query q = mDAO.getEntityManager().createQuery(consulta, VndVentaCabecera.class);
		q.setParameter("fechaInicio", new Timestamp(fechaInicio.getTime()));
		q.setParameter("fechaFin", new Timestamp(fechaFin.getTime()));
		return q.getResultList();
	}

	// Cliente

	public List<VndCliente> findAllCliente() {
		TypedQuery<VndCliente> q = em.createQuery("SELECT v FROM VndCliente v", VndCliente.class);
		return q.getResultList();
	}

	public VndCliente findClienteByCedula(String cedulaCliente) {
		return em.find(VndCliente.class, cedulaCliente);
	}

	public void insertarCliente(VndCliente nuevoCliente) throws Exception {
		em.persist(nuevoCliente);
	}

	public void actualizarCliente(VndCliente edicionCliente) throws Exception {
		VndCliente Cliente = (VndCliente) mDAO.findById(VndCliente.class, edicionCliente.getCedula());

		Cliente.setCedula(edicionCliente.getCedula());
		Cliente.setNombre(edicionCliente.getNombre());
		Cliente.setDireccion(edicionCliente.getDireccion());
		Cliente.setTelefono(edicionCliente.getTelefono());
		mDAO.actualizar(Cliente);
	}

	/*
	 * public void insertarCliente(String cedula, String nombre, String direccion,
	 * String telfono) throws Exception { VndCliente cliente = new VndCliente();
	 * cliente.setCedula(cedula); cliente.setNombre(nombre);
	 * cliente.setDireccion(direccion); cliente.setTelefono(telfono);
	 * 
	 * em.persist(cliente); }
	 */
	public void eliminarCliente(String cedula) {
		VndCliente cliente = findClienteByCedula(cedula);
		if (cliente != null) {
			em.remove(cliente);
		}
	}

	// Facturas
	public List<VntProducto> findAllProductos() {
		TypedQuery<VntProducto> q = em.createQuery("SELECT v FROM VntProducto v", VntProducto.class);
		return q.getResultList();
	}

	public List<VndVentaCabecera> findAllVentaCab() {
		TypedQuery<VndVentaCabecera> q = em.createQuery("SELECT v FROM VndVentaCabecera v", VndVentaCabecera.class);
		return q.getResultList();
	}

	public DTODetalleVenta crearDetalleVenta(int idProducto, double costo, int cantidad) {
		VntProducto pr = em.find(VntProducto.class, idProducto);

		DTODetalleVenta detalleVenta = new DTODetalleVenta();
		detalleVenta.setProducto(pr.getDescripcion());
		detalleVenta.setIdProducto(idProducto);
		detalleVenta.setCosto(costo);
		detalleVenta.setCantidad(cantidad);
		detalleVenta.setSubtotal(costo * cantidad);

		return detalleVenta;
	}

	public double totalDetalleVenta(List<DTODetalleVenta> listaDetalleVenta) {
		double total = 0;
		for (DTODetalleVenta d : listaDetalleVenta)
			total += d.getSubtotal();
		return total;
	}

	public void ventasMultiples(List<DTODetalleVenta> listaDetalleVentas, String cedulaClientes, Date fechaVenta) {
		VndCliente cliente = em.find(VndCliente.class, cedulaClientes);
		VndVentaCabecera cabeceraVenta = new VndVentaCabecera();
		cabeceraVenta.setVndCliente(cliente);
		cabeceraVenta.setFechaVenta(fechaVenta);
		cabeceraVenta.setSubtotal(totalDetalleVenta(listaDetalleVentas));
		cabeceraVenta.setIva(iva(totalDetalleVenta(listaDetalleVentas)));
		cabeceraVenta.setTotal(cabeceraVenta.getIva() + cabeceraVenta.getSubtotal());

		List<VndVentaDetalle> listaDetalle = new ArrayList<VndVentaDetalle>();
		cabeceraVenta.setVndVentaDetalles(listaDetalle);

		for (DTODetalleVenta det : listaDetalleVentas) {
			VndVentaDetalle ventaDetalle = new VndVentaDetalle();
			ventaDetalle.setVndVentaCabecera(cabeceraVenta);
			VntProducto producto = em.find(VntProducto.class, det.getIdProducto());
			ventaDetalle.setVntProducto(producto);
			ventaDetalle.setCantidad(det.getCantidad());
			ventaDetalle.setCosto(det.getCosto());
			producto.setCantidad(actualizarStock(det.getIdProducto(), det.getCantidad()));
			ventaDetalle.setSubtotal(det.getSubtotal());
			listaDetalle.add(ventaDetalle);
		}
		em.persist(cabeceraVenta);
	}

	public int actualizarStock(int idProducto, int cantidad) {
		int stock = 0;
		VntProducto producto = em.find(VntProducto.class, idProducto);
		stock = producto.getCantidad() - cantidad;
		return stock;
	}

	public double iva(double total) {
		double iva = total * 0.12;
		return iva;
	}

	
}
