package moraandinademo.controller.inventario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.encoding.Serializer;

import org.primefaces.PrimeFaces;

import minimarketdemo.controller.JSFUtil;
import moraandinademo.model.core.entities.VntProducto;
import moraandinademo.model.inventario.managers.ManagerInventario;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanInventario implements Serializer {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerInventario managerInverntario;

	private List<VntProducto> listaproducto;
	private VntProducto nuevoProducto;
	private VntProducto edicionProducto;

	public BeanInventario() {
		// TODO Auto-generated constructor stub
	}


	@PostConstruct
	public void inicializar() {
		nuevoProducto = new VntProducto();
		listaproducto = managerInverntario.findAllVntProducto();
		
	}

	public List<VntProducto> getListProductos() {
		return listaproducto;
	}
	

	public String actionMenuNuevoProducto() {
		nuevoProducto = new VntProducto();
		return "producto";
	}

	public void actionSeleccionarEdicionProducto(VntProducto producto) {
		edicionProducto = producto;
	}

	public void actionListenerActualizarEdicionProducto() {
		try {
			managerInverntario.actualizarProducto(edicionProducto);
			listaproducto = managerInverntario.findAllVntProducto();
			JSFUtil.crearMensajeINFO("Producto actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		PrimeFaces.current().executeScript("PF('dialogoEdicion').hide()");
		PrimeFaces.current().ajax().update("form:tabla");
	}

	public void actionListenerInsertarnuevoProducto() {
		try {
			managerInverntario.InsertarProducto(nuevoProducto);
			nuevoProducto = new VntProducto();
			listaproducto = managerInverntario.findAllVntProducto();
			JSFUtil.crearMensajeINFO("Producto insertado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerEliminarProducto(int idProducto) {
		try {
			managerInverntario.eliminarProducto(idProducto);
			listaproducto = managerInverntario.findAllVntProducto();
			JSFUtil.crearMensajeINFO("Producto eliminado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("inventario/bodeguero/inven.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=Reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/sdpalmaq", "sdpalmaq",
					"0402003602");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public List<VntProducto> getListaproducto() {
		return listaproducto;
	}

	public void setListaproducto(List<VntProducto> listaproducto) {
		this.listaproducto = listaproducto;
	}

	public VntProducto getNuevoProducto() {
		return nuevoProducto;
	}

	public void setNuevoProducto(VntProducto nuevoProducto) {
		this.nuevoProducto = nuevoProducto;
	}

	public VntProducto getEdicionProducto() {
		return edicionProducto;
	}

	public void setEdicionProducto(VntProducto edicionProducto) {
		this.edicionProducto = edicionProducto;
	}


	@Override
	public String getMechanismType() {
		// TODO Auto-generated method stub
		return null;
	}
}
