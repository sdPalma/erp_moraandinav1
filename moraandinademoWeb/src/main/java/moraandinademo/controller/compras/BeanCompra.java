package moraandinademo.controller.compras;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import minimarketdemo.controller.JSFUtil;
import moraandinademo.model.compras.dtos.DTODetalleCompra;
import moraandinademo.model.compras.managers.ManagerCompra;
import moraandinademo.model.core.entities.InvProveedor;
import moraandinademo.model.core.entities.PrvCompraCabecera;
import moraandinademo.model.core.entities.VntProducto;

@Named
@SessionScoped
public class BeanCompra implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCompra mc;
	private List<InvProveedor> listaProveedor;
	private List<VntProducto> listaProducto;
	private List<DTODetalleCompra> listaDetalleCompra;
	private List<PrvCompraCabecera> listaCompras;

	private String proveedor;
	private int idpro;
	private int canti;
	private double costo;
	private double total;
	private int idCabCom;
	private VntProducto p;

	public BeanCompra() {
	}

	@PostConstruct
	public void inicializar() {
		listaProveedor = mc.findAllInvProveedor();
		listaProducto = mc.findAllVtnProducto();
		listaCompras = mc.findAllPrvCompraCabeceras();
		listaDetalleCompra = new ArrayList<DTODetalleCompra>();

		System.out.println("" + idCabCom);
	}

	public String actionMenuCompras() {
		listaProveedor = mc.findAllInvProveedor();
		listaProducto = mc.findAllVtnProducto();
		listaCompras = mc.findAllPrvCompraCabeceras();
		listaDetalleCompra = new ArrayList<DTODetalleCompra>();
		return "menu";
	}

	public void actionListenerAgregarDetalleCompra() {
		listaDetalleCompra.add(mc.crearDetalleCompra(idpro, canti, costo));
		JSFUtil.crearMensajeINFO("Producto agregado");
		total = mc.totalDetalleCompra(listaDetalleCompra);
		System.out.println("Guardado detalle");

	}

	public void actionListenerGuardarCompraMultiple() {
		mc.productosMultiples(listaDetalleCompra, proveedor);
		JSFUtil.crearMensajeINFO("Compra Registrada");
		System.out.println("Guardado multiple");
		listaDetalleCompra = new ArrayList<DTODetalleCompra>();
		total = 0;
		inicializar();
	}

	public String actionReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("compra/administrador/reporteCompra.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporteCompra.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/sdpalmaq", "sdpalmaq",
					"0402003602");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";

	}

	public String actionReporteUnico() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("id", idCabCom);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("compra/administrador/reporteU.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporteCompraUnico.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/sdpalmaq", "sdpalmaq",
					"0402003602");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";

	}

	public int getIdCabCom() {
		return idCabCom;
	}

	public void setIdCabCom(int idCabCom) {
		this.idCabCom = idCabCom;
	}

	public List<PrvCompraCabecera> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<PrvCompraCabecera> listaCompras) {
		this.listaCompras = listaCompras;
	}

	public VntProducto getP() {
		return p;
	}

	public void setP(VntProducto p) {
		this.p = p;
	}

	public List<InvProveedor> getListaProveedor() {
		return listaProveedor;
	}

	public void setListaProveedor(List<InvProveedor> listaProveedor) {
		this.listaProveedor = listaProveedor;
	}

	public List<VntProducto> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<VntProducto> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public List<DTODetalleCompra> getListaDetalleCompra() {
		return listaDetalleCompra;
	}

	public void setListaDetalleCompra(List<DTODetalleCompra> listaDetalleCompra) {
		this.listaDetalleCompra = listaDetalleCompra;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public int getIdpro() {
		return idpro;
	}

	public void setIdpro(int idpro) {
		this.idpro = idpro;
	}

	public int getCanti() {
		return canti;
	}

	public void setCanti(int canti) {
		this.canti = canti;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
