package moraandinademo.controller.proveedores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import minimarketdemo.controller.JSFUtil;
import moraandinademo.model.compras.managers.ManagerProveedor;
import moraandinademo.model.compras.managers.ManagerCompra;
import moraandinademo.model.core.entities.InvProveedor;

@SuppressWarnings("cdi-ambiguous-name")
@Named
@SessionScoped
public class BeanProveedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerProveedor mProveedor;
	

	private InvProveedor nuevoProveedor;
	private InvProveedor edicionProveedor;
	private List<InvProveedor> listaProveedores;

	public BeanProveedor() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void incializar() {
		nuevoProveedor = new InvProveedor();
		listaProveedores = mProveedor.findAllInvProveedor();
	}

	public void actionListenerEliminarProveedor(String idInProveedor) {
		try {
			mProveedor.eliminarProveedor(idInProveedor);
			listaProveedores = mProveedor.findAllInvProveedor();
			JSFUtil.crearMensajeINFO("Proveedor eliminado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actionListenerInsertarProveedor() {
		try {
			mProveedor.insertarProveedor(nuevoProveedor);
			nuevoProveedor = new InvProveedor();
			listaProveedores = mProveedor.findAllInvProveedor();
			JSFUtil.crearMensajeINFO("Proveedor Guardado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		PrimeFaces.current().executeScript("PF('dialogoE').hide()");
		PrimeFaces.current().ajax().update("form:tabla");
	}

	
	public void actionSeleccionarEdicionProveedor(InvProveedor Provee) {
		edicionProveedor = Provee;
	}

	public void actionListenerActualizarEdicionProveedor() {
		try {
			mProveedor.actualizarProveedor(edicionProveedor);
			listaProveedores = mProveedor.findAllInvProveedor();
			JSFUtil.crearMensajeINFO("Proveedor actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}

		PrimeFaces.current().executeScript("PF('dialogoEdicion').hide()");
		PrimeFaces.current().ajax().update("form:tabla");
	}

	public InvProveedor getNuevoProveedor() {
		return nuevoProveedor;
	}

	public void setNuevoProveedor(InvProveedor nuevoProveedor) {
		this.nuevoProveedor = nuevoProveedor;
	}

	public InvProveedor getEdicionProveedor() {
		return edicionProveedor;
	}

	public void setEdicionProveedor(InvProveedor edicionProveedor) {
		this.edicionProveedor = edicionProveedor;
	}

	public List<InvProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<InvProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

}
