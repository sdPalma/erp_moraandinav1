package moraandinademo.controller.facturas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.map.HashedMap;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.utils.ModelUtil;
import moraandina.model.ventas.managers.ManagerVentas;
import moraandinademo.model.core.entities.VndCliente;
import moraandinademo.model.core.entities.VndVentaCabecera;
import moraandinademo.model.core.entities.VntProducto;
import moraandinademo.model.ventas.dtos.DTODetalleVenta;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanFacturas implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerVentas managerVentas;

	private List<VntProducto> listaProductos;
	private List<VndCliente> listaClientes;
	private List<VndVentaCabecera> listaVentasCab;
	private List<VndVentaCabecera> listaVentasCabfech;
	private List<DTODetalleVenta> listaDetalleVentas;

	private Date fechaInicio;
	private Date fechaFin;

	private String cedulaClienteSeleccionado;
	private int idProductoSeleccionado;
	private int cantidad;
	private double precio;
	private double total;
	private Date fechaVenta;

	public BeanFacturas() {
		// TODO Auto-generated constructor stub
	}

	public String actionMenuVentas() {
		listaProductos = managerVentas.findAllProductos();
		listaClientes = managerVentas.findAllCliente();
		listaVentasCab = managerVentas.findAllVentaCab();
		listaDetalleVentas = new ArrayList<DTODetalleVenta>();
		return "Facturacion";
	}

	// menu
	public String actionCargarMenuVenta() {
		// obtener la fecha de ayer:
		fechaInicio = ModelUtil.addDays(new Date(), -1);
		// obtener la fecha de hoy:
		fechaFin = new Date();
		listaVentasCabfech = managerVentas.findVentaByFecha(fechaInicio, fechaFin);
		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaVentasCabfech.size());
		return "menu";

	}

	public void actionListenerConsultarVenta() {
		listaVentasCabfech = managerVentas.findVentaByFecha(fechaInicio, fechaFin);
		JSFUtil.crearMensajeINFO("Registros encontrados: " + listaVentasCabfech.size());
	}

	// Maestro Detalle

	public void actionListenerAgregarDetalleVenta() {
		listaDetalleVentas.add(managerVentas.crearDetalleVenta(idProductoSeleccionado, precio, cantidad));
		total = managerVentas.totalDetalleVenta(listaDetalleVentas);
	}

	public void actionListenerGuardarVentaMultiple() {
		managerVentas.ventasMultiples(listaDetalleVentas, cedulaClienteSeleccionado, fechaVenta);
		JSFUtil.crearMensajeINFO("Se registro correctamente");
		listaVentasCab = managerVentas.findAllVentaCab();
		listaDetalleVentas = new ArrayList<DTODetalleVenta>();
		total = 0;
	}

	@SuppressWarnings("deprecation")
	public String actionReporte(int id) {
		Map<String, Object> parametros = new HashedMap<String, Object>();
		parametros.put("id_cabecera", id);
		System.out.println(id);
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("venta/vendedor/facturav1.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=factura.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/sdpalmaq", "sdpalmaq",
					"0402003602");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public List<VntProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<VntProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<VndCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<VndCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<VndVentaCabecera> getListaVentasCab() {
		return listaVentasCab;
	}

	public void setListaVentasCab(List<VndVentaCabecera> listaVentasCab) {
		this.listaVentasCab = listaVentasCab;
	}

	public List<DTODetalleVenta> getListaDetalleVentas() {
		return listaDetalleVentas;
	}

	public void setListaDetalleVentas(List<DTODetalleVenta> listaDetalleVentas) {
		this.listaDetalleVentas = listaDetalleVentas;
	}

	public String getCedulaClienteSeleccionado() {
		return cedulaClienteSeleccionado;
	}

	public void setCedulaClienteSeleccionado(String cedulaClienteSeleccionado) {
		this.cedulaClienteSeleccionado = cedulaClienteSeleccionado;
	}

	public int getIdProductoSeleccionado() {
		return idProductoSeleccionado;
	}

	public void setIdProductoSeleccionado(int idProductoSeleccionado) {
		this.idProductoSeleccionado = idProductoSeleccionado;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<VndVentaCabecera> getListaVentasCabfech() {
		return listaVentasCabfech;
	}

	public void setListaVentasCabfech(List<VndVentaCabecera> listaVentasCabfech) {
		this.listaVentasCabfech = listaVentasCabfech;
	}

}
