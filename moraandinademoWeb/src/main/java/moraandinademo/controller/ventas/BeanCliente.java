package moraandinademo.controller.ventas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.map.HashedMap;
import org.primefaces.PrimeFaces;

import minimarketdemo.controller.JSFUtil;
import moraandina.model.ventas.managers.ManagerVentas;
import moraandinademo.model.core.entities.VndCliente;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerVentas managerVentas;
	private List<VndCliente> listaClientes;
	private VndCliente nuevoCliente;
	private VndCliente edicionCliente;

	public BeanCliente() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void inicializar() {
		nuevoCliente = new VndCliente();
		listaClientes = managerVentas.findAllCliente();
	}

	/*
	 * public void actionListenerInsertarCliente() { try {
	 * managerVentas.insertarCliente(cedula, nombre, direccion, telefono); ;
	 * listaClientes = managerVentas.findAllCliente();
	 * JSFUtil.crearMensajeINFO("Cliente Creado"); nuevoCliente = new VndCliente();
	 * } catch (Exception e) { // TODO: handle exception
	 * JSFUtil.crearMensajeERROR(e.getMessage()); e.printStackTrace(); } }
	 */

	public void actionListenerInsertarCliente() {
		try {
			managerVentas.insertarCliente(nuevoCliente);
			nuevoCliente = new VndCliente();
			listaClientes = managerVentas.findAllCliente();
			listaClientes = managerVentas.findAllCliente();
			JSFUtil.crearMensajeINFO("Cliente Guardado");

		} catch (Exception e) { // TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		PrimeFaces.current().executeScript("PF('dialogoE').hide()");
		PrimeFaces.current().ajax().update("form:tabla");
	}

	public void actionEliminarCliente(String cedulaCliente) {
		managerVentas.eliminarCliente(cedulaCliente);
		listaClientes = managerVentas.findAllCliente();
		JSFUtil.crearMensajeINFO("Cliente Eliminado");
	}

	public void actionSeleccionarEdicionCliente(VndCliente cliente) {
		edicionCliente = cliente;
	}

	public void actionListenerActualizarCliente() {
		try {
			managerVentas.actualizarCliente(edicionCliente);
			listaClientes = managerVentas.findAllCliente();
			JSFUtil.crearMensajeINFO("Cliente actualizado");
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		PrimeFaces.current().executeScript("PF('dialogoEdicion').hide()");
		PrimeFaces.current().ajax().update("form:tabla");
	}

	@SuppressWarnings("deprecation")
	public String actionReporte() {
		Map<String, Object> parametros = new HashedMap<String, Object>();
		/*
		 * parametros.put("p_titulo_principal",p_titulo_principal);
		 * parametros.put("p_titulo",p_titulo);
		 */
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("venta/vendedor/ventas_reporte.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/sdpalmaq", "sdpalmaq",
					"0402003602");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public List<VndCliente> getListaClientes() {
		return listaClientes;
	}

	public VndCliente getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(VndCliente nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public void setListaClientes(List<VndCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public VndCliente getEdicionCliente() {
		return edicionCliente;
	}

	public void setEdicionCliente(VndCliente edicionCliente) {
		this.edicionCliente = edicionCliente;
	}

}
