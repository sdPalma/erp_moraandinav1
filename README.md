# ERP_moraandinaV1

Repositorio  sin fallas 

# Descripción del producto
El presente proyecto moraandina es una aplicación web que simula el proceso de compra, venta, inventario de productos de una empresa  realizada en el entorno de desarrollo eclipse  en lenguaje java, en donde se utilizó el servidor de Wildfly 12 y base de datos sql en el gestor de base de datos PgAdmin 4. Además, cuenta con un gestor de reportes en formato PDF, utilizando la herramienta de Jasper (6.17.0) y librerías correspondientes. Unas vez establecidas las distintas herramientas se realiza la creación de base de datos con su nombre especifico para enlazarla a eclipse y realizar los controladores de cada proceso que se estableció.

# Universidad Técnica del Norte
Personal de trabajo

* Anthony Mora-Scrum master - ajmorae@utn.edu.ec
* Stalyn Palma-Desarrollador - sdpalmaq@utn.edu.ec
* David Quilsimba-Desarrollador - dmquilsimaq@utn.edu.ec
* Omar Simbaña-Desarrollador - bosimbanaa@utn.edu.ec
* Brayan Velasco-Desarrollador - bavelascor@utn.edu.ec
